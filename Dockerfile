FROM bitshares/bitshares-core

ADD healthcheck.sh /usr/local/bin/healthcheck.sh
RUN chmod a+x /usr/local/bin/healthcheck.sh
# Make Docker send SIGINT instead of SIGTERM to the daemon

STOPSIGNAL SIGINT