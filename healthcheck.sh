#!/bin/bash

BLOCKCHAIN_TIME=$(
    curl --silent --max-time 3 \
        --data '{"jsonrpc":"2.0","id":1,"method":"get_dynamic_global_properties"}' \
        localhost:8090 | python -c 'import sys, json; sys.tracebacklimit = 0; print json.load(sys.stdin)["result"]["time"]'
)

if [[ ${BLOCKCHAIN_TIME} == "null" ]]; then
  echo "The node is currently not responding."
  exit 1
fi

if [[ ! -z  "$BLOCKCHAIN_TIME" ]]; then
  BLOCKCHAIN_SECS=`date -d $BLOCKCHAIN_TIME +%s`
  CURRENT_SECS=`date +%s`

  # if we're within 20 seconds of current time, call it synced and report healthy
  BLOCK_AGE=$((${CURRENT_SECS} - ${BLOCKCHAIN_SECS}))
  if [[ ${BLOCK_AGE} -le 20 ]]; then
    echo "Block age is less than 20 seconds old, this node is considered healthy."
    exit 0
  else
    echo "The node is responding but block chain age is $BLOCK_AGE seconds old"
    exit 1
  fi
else
  echo "The node is currently not responding."
    exit 1
 fi
